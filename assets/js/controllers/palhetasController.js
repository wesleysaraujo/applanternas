angular.module('lanternaCerta')
	.controller('palhetasController', function($scope, palhetasAPI, $route) {
		
		$scope.appTitle = "Encontre a palheta certa para o seu veículo";

		$scope.route = $route;
		
		var db = [];
		var modeloMarca = [];
		var anosModelo = [];

		$scope.marcas = [];
		$scope.modelos = [];
		$scope.anos = [];
		$scope.farol_baixo = "";
		$scope.farol_alto = "";
		$scope.farol_neblina = "";

		var carregaLista = function(){
			palhetasAPI.getPalhetas().success(function(data){
				var lookup = {};
				var result = [];
				var marcas = [];
				//console.log(data);
				db = data;
				for (var item, i = 0; item = data[i++];) {
				  var name = item.marca;

				  if (!(name in lookup)) {
				    lookup[name] = 1;
				    marcas.push(name);
				  }
				}

				$scope.marcas = marcas;
			});
		}

		carregaLista();

		$scope.exibeModelos = function(marca){
			console.log(marca);
			//console.log(db);
			var lookup = {};
			var result = [];
			var modelos = [];

			var carroEscolhido = db.filter(function(index) {
				return index.marca === marca;
			});
			
			modelosMarca = carroEscolhido;

			for (var item, i = 0; item = carroEscolhido[i++];) {
				  var name = item.modelo;

				  if (!(name in lookup)) {
				    lookup[name] = 1;
				    modelos.push(name);
				  }
				}

			$scope.modelos = modelos;


		}

		$scope.exibeAnos = function(modelo){
			console.log(modelo);
			//console.log(modelosMarca);

			var modeloEscolhido = modelosMarca.filter(function(index) {
				return index.modelo === modelo;
			});

			console.log(modeloEscolhido);

			anosModelo = modeloEscolhido;
			
			$scope.anos = modeloEscolhido;
			
		}

		$scope.exibeFarois = function(ano){
			console.log(ano);

			$scope.lado_direito = ano.lado_direito;
			$scope.lado_esquerdo = ano.lado_esquerdo;
			$scope.palheta_traseira = ano.palheta_traseira;
		}
	});
