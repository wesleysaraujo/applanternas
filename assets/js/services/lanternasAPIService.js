angular.module('lanternaCerta')
.factory('lanternasAPI', function($http){
	var _getLanternas = function(){
		return $http.get('assets/js/data/listagem.json');
	}

	return {
		getLanternas : _getLanternas,
	}
})
.factory('palhetasAPI', function($http){
	var _getPalhetas = function(){
		return $http.get('assets/js/data/palhetas.json');
	}

	return {
		getPalhetas : _getPalhetas,
	}
});
