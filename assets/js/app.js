angular.module('lanternaCerta', ['ngRoute', 'ui.router']).config(['$routeProvider', '$locationProvider', '$urlRouterProvider', function ($routeProvider, $locationProvider, $urlRouterProvider) {

    $routeProvider.when('/', {
        templateUrl: 'templates/lampadas.html',
        controller: 'lanternasController',
        activetab: 'lampadas'
    });

    $routeProvider.when('/lampadas', {
        templateUrl: 'templates/lampadas.html',
        controller: 'lanternasController',
        activetab: 'lampadas'
    });
    
    $routeProvider.when('/palhetas', {
        templateUrl: 'templates/palhetas.html',
        controller: 'palhetasController',
        activetab: 'palhetas'
    });

    $urlRouterProvider.otherwise('/');
}]);
