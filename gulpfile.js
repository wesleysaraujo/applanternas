var gulp = require("gulp");
var less = require('gulp-less');
var path = require('path');

var paths = {
  less: ['./src/less/**/*.less']
};
 
gulp.task('default', ['less']);

gulp.task('less', function () {
  return gulp.src('./src/less/**/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./assets/css'));
});

gulp.task('watch', function() {
  gulp.watch(paths.less, ['less']);
});